﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float moveSpeed = 7;

    new Rigidbody rigidbody;
    bool disabled;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        Guard.OnGuardHasSpottedPlayer += Disable;
    }

    void FixedUpdate()
    {
        Vector3 inputDirection = Vector3.zero;
        if (!disabled)
        {
            inputDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;
        }
        float inputMagnitude = inputDirection.magnitude;;

        //velocity = transform.forward * moveSpeed * inputMagnitude;
        rigidbody.velocity = inputDirection * moveSpeed;
        //rigidbody.MoveRotation(Quaternion.Euler(Vector3.up * angle));
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finish")
        {
            Disable();
            GameController.instance.ShowGameWinUI();
            Disable();
        }
    }

    public void Disable()
    {
        disabled = true;
    }

}